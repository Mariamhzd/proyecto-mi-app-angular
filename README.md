My App

Proyecto final del módulo de Angular realizado por María Martín Hernández, aplicando los conocimientos adquiridos en clase y llamando a una API creada por mí.

Instalación / Arranque Proyecto:

Levantar el back-api -> Sitúate con la terminal en la carpeta back-api.
Ejecutar:
npm i json-server (la primera vez que lo abrimos)
npm run server

Levantar el proyecto -> Sitúate en la carpeta del proyecto: final-app
Ejecuta npm i (solo la primera vez)
Situate en la carpeta /src/app
Ejecuta ng serve
Navega a http://localhost:4200/. La app se cargará automáticamente si haces algun cambio.

Proyecto:

Es mi primer proyecto creado con Angular. Version 10.0.4.

El proyecto lo he llamado Mi grupo, se trata de una página de un grupo musical donde hay varios apartados entre ellos Music y Merchan que están llamando a una api creada por mí.
Trabajo con typescript, creando comunicación entre componentes, rutas, directivas y template, formularios reactivos, servicios y observables

1. El primer apartado HOME, tiene tres componentes hijos 
   1.1 Home-body donde tenemos un videoclipt, usando la librería  de reproductor de youtube: @angular/youtube-player
   1.2 Home-gallery donde he creado un carrosuel de fotos, para el que he utilizado la librería Ng2Carouselamos
   1.3 Home-form con un formulario de regristro.

2. El segundo apartado MUSIC.
   Toda la información de Music está llamando a la Back-api.
   En la portada principal es un listado con los discos del grupo, donde si pulsas en la caratula de cada disco te enlaza al   detalle del mismo. 
   Dentro del detalle hay una función GoBack para volver a la página anterior.

3. El tercer apartado MERCHAN.
   Es un poco igual que el apartado Music, toda la información está llamando a la Back-Api
   La pagina principal de merchan es un listado con los productos que se pueden comprar y pulsando encima de
   cada producto te enlaza al detalle.
   Dentro del detalle hay una función GoBack para volver a la página anterior.

4. Aparte también tiene dos componentes principales Header y Footer.
   Dentro del apartado Header, están las rutas al resto de apartados de la aplicación.





