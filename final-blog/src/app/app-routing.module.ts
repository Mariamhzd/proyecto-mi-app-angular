import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'music',
    loadChildren: () =>
      import('./pages/music/music.module').then((m) => m.MusicModule),
  },
  {
    path: 'merchan',
    loadChildren: () =>
      import('./pages/merchan/merchan.module').then((m) => m.MerchanModule),
  },
  {
    path: 'about',
    loadChildren: () =>
      import('./pages/about/about.module').then((m) => m.AboutModule),
  },
  {
    path: 'detail-music/:id',
    loadChildren: () =>
      import('./pages/detail-music/detail-music.module').then((m) => m.DetailMusicModule),
  },
  {
    path: 'detail-merchan/:id',
    loadChildren: () =>
      import('./pages/detail-merchan/detail-merchan.module').then((m) => m.DetailMerchanModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
