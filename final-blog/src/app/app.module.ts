import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

import { MusicListServiceService } from './services/music-list-service.service';
import { MerchandisingService } from './services/merchandising.service';

import { YouTubePlayerModule } from "@angular/youtube-player";

import { DetailMusicComponent } from './pages/detail-music/detail-music.component';
import { DetailMerchanComponent } from './pages/detail-merchan/detail-merchan.component';

import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DetailMusicComponent,
    DetailMerchanComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    YouTubePlayerModule,
    ReactiveFormsModule
  ],
  providers: [MusicListServiceService, MerchandisingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
