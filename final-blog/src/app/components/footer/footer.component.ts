import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  logo: string;
  footer: string;
  constructor() { 
    this.logo ='MI GRUPO';
    this.footer = '© 2020 Mi Grupo | Privacy Policy';
  }

  ngOnInit(): void {
  }

}
