export interface DiscographyList {
    id: number;
    name: string;
    age: number;
    discography: string;
    duration: string;
    image: string;
}
export interface DiscographyDetail {
    id: number;
    name: string;
    age: number;
    discography: string;
    image: string;
    duration: string;
    songList: string [];
}

export interface OtherAlbums {
    id: number;
    name: string;
    age: number;
    gender: string;
    duration: string;
    image: string;
    songList: string [];
}

export interface Merch {
    id: number;
    imagen: string;
    product: string;
    price: string;
}

export interface MerchDetail {
    id: number;
    imagen: string;
    product: string;
    price: string;
    code: string;
    description: string;
}