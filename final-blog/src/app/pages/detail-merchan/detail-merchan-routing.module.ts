import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailMerchanComponent } from './detail-merchan.component';

const routes: Routes = [{ path: '', component: DetailMerchanComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailMerchanRoutingModule { }
