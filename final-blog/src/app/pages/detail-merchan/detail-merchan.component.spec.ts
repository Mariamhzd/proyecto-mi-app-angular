import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailMerchanComponent } from './detail-merchan.component';

describe('DetailMerchanComponent', () => {
  let component: DetailMerchanComponent;
  let fixture: ComponentFixture<DetailMerchanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailMerchanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailMerchanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
