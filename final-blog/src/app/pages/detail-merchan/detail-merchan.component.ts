import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { MerchandisingService } from './../../services/merchandising.service';
import { MerchDetail } from '../../models/interface-api';

@Component({
  selector: 'app-detail-merchan',
  templateUrl: './detail-merchan.component.html',
  styleUrls: ['./detail-merchan.component.scss']
})
export class DetailMerchanComponent implements OnInit {
  public detailMerchan: MerchDetail | any;
  public itemId: string | null = null;
  constructor(
    private merchandisingService: MerchandisingService,
    private location: Location,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getDetailMerchan();
  }

  public getDetailMerchan(): void  {
    this.route.paramMap.subscribe((params) => {
      this.itemId = params.get('id');
    });
    this.merchandisingService.getDetailMerchan(Number(this.itemId)).subscribe(
      (data: MerchDetail) => {
        this.detailMerchan = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
  public goBack(): void {
    this.location.back();
  }

}
