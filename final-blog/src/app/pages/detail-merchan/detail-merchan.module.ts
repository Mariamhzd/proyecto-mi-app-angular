import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailMerchanRoutingModule } from './detail-merchan-routing.module';
import { DetailMerchanComponent } from './detail-merchan.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DetailMerchanRoutingModule
  ]
})
export class DetailMerchanModule { }
