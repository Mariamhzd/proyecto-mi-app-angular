import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { MusicListServiceService } from './../../services/music-list-service.service';
import { DiscographyDetail } from '../../models/interface-api';


@Component({
  selector: 'app-detail-music',
  templateUrl: './detail-music.component.html',
  styleUrls: ['./detail-music.component.scss']
})
export class DetailMusicComponent implements OnInit {
  public detailMusic: DiscographyDetail | any;
  public itemId: string | null = null;
  constructor(
    private musicListServiceService: MusicListServiceService,
    private location: Location,
    private route: ActivatedRoute
    ) {}

  ngOnInit(): void {
    this.getDetailMusic();
  }

  // creo una funcion que atacara el servicio pasando el id
  public getDetailMusic(): void {
    this.route.paramMap.subscribe((params) => {
      this.itemId = params.get('id');
    });
    this.musicListServiceService.getDetailMusic(Number(this.itemId)).subscribe(
      (data: DiscographyDetail) => {
      this.detailMusic = data;
    },
    (err) => {
      console.error(err.message);
    }
    );
  }

  public goBack(): void {
    this.location.back();
  }
}
