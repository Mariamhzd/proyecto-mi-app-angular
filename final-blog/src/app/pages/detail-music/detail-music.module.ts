import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailMusicRoutingModule } from './detail-music-routing.module';
import { DetailMusicComponent} from './detail-music.component';


@NgModule({
  declarations: [ ],
  imports: [
    CommonModule,
    DetailMusicRoutingModule
  ]
})
export class DetailMusicModule { }
