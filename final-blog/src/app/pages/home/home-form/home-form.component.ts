import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';;
import { Iform , Icountry } from './models/Iform';



@Component({
  selector: 'app-home-form',
  templateUrl: './home-form.component.html',
  styleUrls: ['./home-form.component.scss']
})
export class HomeFormComponent implements OnInit {
  public formContact: FormGroup;
  // tslint:disable-next-line: no-inferrable-types
  public submitted: boolean = false;
  public country: any = [];

  constructor(private formBuilder: FormBuilder) {
    this.formContact = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      surname: ['', [Validators.required, Validators.minLength(3)]],
      age: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.maxLength(20)]],
      country: ['', [Validators.required]],
      policy: ['', [Validators.required]],
    });
    this.country = this.getCountry();
   }

  ngOnInit(): void {/* Empty */}

  getCountry(): Icountry [] {
    return [
      { id: '1', name: 'Spain'},
      { id: '2', name: 'Germany'},
      { id: '3', name: 'United Kingdom'},
      { id: '5', name: 'U.S.A'},
      { id: '6', name: 'Argentina'},
      { id: '7', name: 'Uruguay'},
    ];
  }

  public onSubmit(): void {
    this.submitted = true;
    if (this.formContact?.valid){
      const fansContact: Iform = {
        name: this.formContact.get('name')?.value,
        surname: this.formContact.get('surname')?.value,
        age: this.formContact.get('age')?.value,
        email: this.formContact?.get('email')?.value,
        country: this.formContact?.get('country')?.value,
        policy: this.formContact.get('policy')?.value,
      };
      console.log(fansContact);      
    };
    this.formContact.reset();
    this.submitted = false;
  }
}





