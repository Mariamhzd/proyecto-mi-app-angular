export interface Iform {
    name: string;
    surname: string;
    age: number;
    email: string;
    country: Icountry;
    policy: boolean;
}

export interface Icountry {
    id: string;
    name: string;
}