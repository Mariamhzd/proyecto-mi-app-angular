import { Component, Input, OnInit } from '@angular/core';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';

@Component({
  selector: 'app-home-gallery',
  templateUrl: './home-gallery.component.html',
  styleUrls: ['./home-gallery.component.scss']
})
export class HomeGalleryComponent implements OnInit {
   tittle: string; 
   items: Array<any> = [];
  constructor() {
    this.tittle = 'FOTOS DE NUESTROS ÚLTIMOS CONCIERTOS'
    this.items = [
      { img: '../../../../assets/fotos/concierto1.jpg'},
      { img: '../../../../assets/fotos/concierto2.jpg'},
      { img: '../../../../assets/fotos/concierto3.jpg'},
      { img: '../../../../assets/fotos/concierto4.jpg'},
      { img: '../../../../assets/fotos/concierto5.jpg'},
      { img: '../../../../assets/fotos/concierto6.jpg'},
    ];
   }

  ngOnInit(): void {
  }

}
