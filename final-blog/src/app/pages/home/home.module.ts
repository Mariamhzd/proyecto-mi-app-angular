import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HomeBodyComponent } from './home-body/home-body.component';

import {YouTubePlayerModule} from '@angular/youtube-player';
import { HomeGalleryComponent } from './home-gallery/home-gallery.component';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { HomeFormComponent } from './home-form/home-form.component';

import { ReactiveFormsModule } from '@angular/forms';


// import { SwiperModule } from 'ngx-swiper-wrapper';
// import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
// import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

// const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
//   direction: 'horizontal',
//   slidesPerView: 'auto'
// };


@NgModule({
  declarations: [HomeComponent, HomeBodyComponent, HomeGalleryComponent, HomeFormComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    YouTubePlayerModule,
    Ng2CarouselamosModule,
    ReactiveFormsModule
    // SwiperModule
  ],
  // providers: [
  //   {
  //     provide: SWIPER_CONFIG,
  //     useValue: DEFAULT_SWIPER_CONFIG
  //   }
  // ] 
})
export class HomeModule { }
