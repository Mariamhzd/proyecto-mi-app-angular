import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MerchanComponent } from './merchan.component';

const routes: Routes = [{ path: '', component: MerchanComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchanRoutingModule { }
