import { MerchandisingService } from './../../services/merchandising.service';
import { Merch } from './../../models/interface-api';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-merchan',
  templateUrl: './merchan.component.html',
  styleUrls: ['./merchan.component.scss']
})
export class MerchanComponent implements OnInit {

  public allMerchan: Merch [] = [];

  constructor(private MerchandisingService: MerchandisingService) {}

  ngOnInit(): void {
    this.getMerchan();
  }
  public getMerchan(): void {this.MerchandisingService.getMerchan().subscribe(
    (data: Merch[]) => {
      this.allMerchan = data;
    },
    (err) => {
      console.error(err.message);
    }
  );
 }
}
