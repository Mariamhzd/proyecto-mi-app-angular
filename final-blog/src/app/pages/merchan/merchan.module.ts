import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MerchanRoutingModule } from './merchan-routing.module';
import { MerchanComponent } from './merchan.component';


@NgModule({
  declarations: [MerchanComponent],
  imports: [
    CommonModule,
    MerchanRoutingModule
  ]
})
export class MerchanModule { }
