import {  DiscographyList } from './../../models/interface-api';
import { Component, OnInit } from '@angular/core';
import { MusicListServiceService } from './../../services/music-list-service.service';


@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.scss']
})
export class MusicComponent implements OnInit {
  public allMusic: DiscographyList [] = [];

  constructor(private musicListServiceService: MusicListServiceService) {}

    ngOnInit(): void {
      this.getMusic();
    }
    public getMusic(): void {this.musicListServiceService.getMusic().subscribe(
      (data: DiscographyList[]) => {
        this.allMusic = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
}



