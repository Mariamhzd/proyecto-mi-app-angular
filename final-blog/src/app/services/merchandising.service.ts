import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Merch, MerchDetail } from './../models/interface-api';

@Injectable({
  providedIn: 'root'
})
export class MerchandisingService {
  private merchanUrl = 'http://localhost:3000/MerchanList';
  private merchanUrlDetail = 'http://localhost:3000/MerchanListDetail'

  public merchanList: Merch[] = [];
  constructor(private http: HttpClient) {}

  public getMerchan(): Observable<Merch[]> {​​​​​​
    return this.http.get(this.merchanUrl).pipe(
      map((response: any) => {​​​​​​​
        if (!response){​​​​​​​
          throw new Error('Value expected!');
        }​​​​​​​else{​​​​​​​
          console.log(response);
          return response;
        }​​​​​​​
      }​​​​​​​),
      catchError((err) => {​​​​​​​
        throw new Error(err.message);
      }​​​​​​​)
   // tslint:disable-next-line: semicolon
   )
  }
  
  // creo la función para ver el detalle

  getDetailMerchan(id:number): Observable<MerchDetail> {
    return this.http.get(`${this.merchanUrlDetail}/${id}`).pipe(
      map((response: any) => {
        if (!response){
          throw new Error('Value expected!');
        }else{
          console.log(response);
          return response;
        }
        }),
        catchError((err) => {
          throw new Error(err.message);
        })
      );
  }
}
