import { TestBed } from '@angular/core/testing';

import { MusicListServiceService } from './music-list-service.service';

describe('MusicListServiceService', () => {
  let service: MusicListServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MusicListServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
