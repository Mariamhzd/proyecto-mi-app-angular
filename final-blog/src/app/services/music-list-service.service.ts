import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { DiscographyList, DiscographyDetail} from './../models/interface-api';


@Injectable({
  providedIn: 'root'
})

export class MusicListServiceService {
  private musicUrl  = 'http://localhost:3000/DiscographyList';
  private musicUrlDetail = 'http://localhost:3000/DiscographyDetail';

  public musicList: DiscographyList[] = [];
  constructor(private http: HttpClient) {}

  public getMusic(): Observable<DiscographyList[]> {​​​​​​
      return this.http.get(this.musicUrl).pipe(
        map((response: any) => {​​​​​​​
          if (!response){​​​​​​​
            throw new Error('Value expected!');
          }​​​​​​​else{​​​​​​​
            console.log(response);
            return response;
          }​​​​​​​
        }​​​​​​​),
        catchError((err) => {​​​​​​​
          throw new Error(err.message);
        }​​​​​​​)
     // tslint:disable-next-line: semicolon
     )
    }
    // creo la función para el detalle//
  getDetailMusic(id: number): Observable<DiscographyDetail> {
    return this.http.get(`${this.musicUrlDetail}/${id}`).pipe(
      map((response: any) => {
        if (!response){​​​​​​​
          throw new Error('Value expected!');
        }​​​​​​​else{​​​​​​​
          console.log(response);
          return response;
        }
      }),
      catchError((err) => {​​​​​​​
        throw new Error(err.message);
      }​​​​​​​)
    );
  }
}

